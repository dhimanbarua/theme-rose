<!-- BLOG ARTICLE -->
				<article class="blog-article">
					<div class="blog-article-audio">
						
						<?php echo wp_oembed_get(get_post_meta(get_the_ID(), '_for-audio', true)); ?>

					</div>
					<div class="blog-article-content">
						<!-- BLOG ARTICLE HEADER -->
						<div class="blog-article-header media">
							<span class="blog-article-media-type cl">
								<i class="icon-pen2"></i>
							</span>
							<div class="bd">
								<h2 class="blog-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="blog-article-info">
									<span class="blog-article-date">
										Posted at: 
										<a href="#"><?php the_time('d M y'); ?></a>
									</span>
									<span class="blog-article-tags">
										Posted in: 
										<a href="#"><?php the_tags(); ?></a>
										
									</span>
								</div>
							</div>
						</div><!-- BLOG ARTICLE HEADER -->
						<p>
							<?php the_content(); ?>
						</p>
						<!-- BLOG ARTICLE FOOTER -->
						<div class="blog-article-footer">
							<div class="blog-article-author">
								<img src="images/team/1.jpg" alt="">
								<h4 class="blog-article-author-name"><a href="#"><?php the_author(); ?></a></h4>
							</div>
							<div class="blog-article-nav">
								<span class="blog-article-nav-item">
									<a href="#"><?php comments_number(); ?></a>
								</span>
								<span class="blog-article-nav-item">
									<a href="<?php the_permalink(); ?>">Read More &rarr;</a>
								</span>
							</div>
						</div><!-- BLOG ARTICLE FOOTER -->
					</div>
				</article><!--  -->