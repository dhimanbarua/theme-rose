<!DOCTYPE html>
<html <?php language_attributes(); ?>>


<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Mobile Devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<?php wp_head(); ?>
</head <?php body_class(); ?>>
<body class="home">
	<!--
	<div id="preloader">
		<div id="status" class="animated infinite pulse"></div>
	</div>
	-->
	<!--********************** HEADER SECTION ************************* -->
	<header class="header-fw">
		<div class="container">
			<div class="header-container col-md-12">
				<!-- LOGO -->
				<h1 class="logo"><a href="<?php home_url(); ?>"><img src="<?php global $redux_rose; echo $redux_rose['logo-upload']['url']; ?>" alt=""></a></h1>
				<div class="nav-container">
					<!-- PRIMARY NAVIGATION -->
					<nav class="primary-navigation">
						<?php 
							wp_nav_menu(array(
								'theme_location' => 'main-menu',
								'menu_class'	 =>	'nav-menu',
								'container'		 =>	'',
								'walker'		 => new Single_Page_Walker()
							));
						?>
						<!-- <ul id="nav-menu">
							<li>
								<a href="#intro">Home</a>
								
							</li>
							<li><a href="#about">About</a></li>
							<li>
								<a href="#portfolio">Portfolio</a>
								<ul class="mega-menu mega-5-col">
									<li class="mega-menu-col">
										<h3 class="mega-menu-col-title">Portfolio Style One</h3>
										<ul>
											<li><a href="portfolio-2-col.html">Portfolio 2 Col</a></li>
											<li><a href="portfolio-3-col.html">Portfolio 3 Col</a></li>
											<li><a href="portfolio-full-4-col.html">Portfolio 4 Col</a></li>
											<li><a href="portfolio-full-5-col.html">Portfolio 5 Col</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<h3 class="mega-menu-col-title">Portfolio Style two</h3>
										<ul>
											<li><a href="portfolio-style-2-2-col.html">Portfolio 2 Col</a></li>
											<li><a href="portfolio-style-2-3-col.html">Portfolio 3 Col</a></li>
											<li><a href="portfolio-style-2-4-col.html">Portfolio 4 Col</a></li>
											<li><a href="portfolio-style-2-5-col.html">Portfolio 5 Col</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<h3 class="mega-menu-col-title">Portfolio Style three</h3>
										<ul>
											<li><a href="portfolio-style-3-2-col.html">Portfolio 2 Col</a></li>
											<li><a href="portfolio-style-3-3-col.html">Portfolio 3 Col</a></li>
											<li><a href="portfolio-style-3-4-col.html">Portfolio 4 Col</a></li>
											<li><a href="portfolio-style-3-5-col.html">Portfolio 5 Col</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<h3 class="mega-menu-col-title">Portfolio Single</h3>
										<ul>
											<li><a href="portfolio-single.html">Portfolio Single</a></li>
											<li><a href="portfolio-single-two.html">Portfolio Single Two</a></li>
											<li><a href="portfolio-single-three.html">Portfolio Three</a></li>
											<li><a href="portfolio-single-four.html">Portfolio Single Four</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<h3 class="mega-menu-col-title">Portfolio Single 2</h3>
										<ul>
											<li><a href="portfolio-single-five.html">Portfolio Single Five</a></li>
											<li><a href="portfolio-single-video.html">Portfolio Single Video</a></li>
											<li><a href="portfolio-single-gallrey.html">Portfolio Single Gallrey</a></li>
											<li><a href="portfolio-single-gallrey-2.html">Portfolio Single Gallrey 2</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li class="selected">
								<a href="#blog">Blog</a>
								<ul>
									<li><a href="blog.html">Blog Standard</a></li>
									<li><a href="blog-sidebar.html">Blog with Sidebar</a></li>
									<li><a href="blog-cover.html">Blog with Cover</a></li>
									<li><a href="blog-masonry.html">Blog Masonry</a></li>
									<li><a href="blog-single-page-standard.html">Blog single page</a></li>
									<li><a href="blog-single-page.html">Blog single image page</a></li>
									<li><a href="blog-single-page-quote.html">Blog single page quote</a></li>
									<li><a href="blog-single-page-gallrey.html">Blog single page gallrey</a></li>
									<li><a href="blog-single-page-audio.html">Blog single page audio</a></li>
									<li><a href="blog-single-page-video.html">Blog single page video</a></li>
								</ul>
							</li>
							<li>
								<a href="#blog">Shop</a>
								<ul>
									<li><a href="shop-2-col-sidebar.html">Shop 2 col sidebar</a></li>
									<li><a href="shop.html">Shop 3 col</a></li>
									<li><a href="shop-3-col-sidebar.html">Shop 3 col sidebar</a></li>
									<li><a href="shop-4-col.html">Shop 4 col</a></li>
									<li><a href="shop-single.html">Shop single</a></li>
									<li><a href="shop-cart.html">Shop cart</a></li>
								</ul>
							</li>
							<li>
								<a href="#">Elements</a>
								<ul class="mega-menu mega-4-col">
									<li class="mega-menu-col">
										<ul>
											<li><a href="framework-accordions.html"><i class="icon-list2"></i>Accordions</a></li>
											<li><a href="framework-animated-images.html"><i class="icon-pictures"></i>Animated images</a></li>
											<li><a href="framework-banner.html"><i class="icon-popup"></i>Banner</a></li>
											<li><a href="framework-brands.html"><i class="icon-cc"></i>Brands</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<ul>
											<li><a href="framework-buttons.html"><i class="icon-cog"></i>Buttons</a></li>
											<li><a href="framework-counter.html"><i class="icon-star22"></i>Counter</a></li>
											<li><a href="framework-features.html"><i class="icon-palette"></i>Features</a></li>
											<li><a href="framework-gallrey.html"><i class="icon-photo"></i>Gallrey slider</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<ul>
											<li><a href="framework-grid.html"><i class="icon-layout"></i>Grid</a></li>
											<li><a href="framework-lists.html"><i class="icon-list"></i>Lists</a></li>
											<li><a href="framework-parallax.html"><i class="icon-screen"></i>Parallax</a></li>
											<li><a href="framework-progressbar.html"><i class="icon-progress-2"></i>Progressbar</a></li>
										</ul>
									</li>
									<li class="mega-menu-col">
										<ul>
											<li><a href="framework-tabs.html"><i class="icon-browser"></i>Tabs</a></li>
											<li><a href="framework-team.html"><i class="icon-users"></i>Team</a></li>
											<li><a href="framework-testimonials.html"><i class="icon-quote"></i>Testimonials</a></li>
											<li><a href="framework-typography.html"><i class="icon-tumblr"></i>Typography</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<li><a href="#contact">Contact</a></li>
						</ul> -->
					</nav>
					<!-- TOOL NAV -->
					<nav class="tool-navigation">
						<button class="search-button"><i class="icon-search2"></i></button>
						<button class="cart-button"><i class="icon-cart"></i> <span class="cart-item-count">8</span></button>
					</nav>
					<span class="responsive-trigger"><i class="icon-list2"></i></span>
				</div>
			</div>
		</div>
		<!-- RESPONSIVE NAVIGATION -->
		<nav class="responsive-navigation">
			<ul>
				<li>
					<a href="#intro">Home</a>
					<span class="submenu-trigger"><i class="icon-arrow-down5"></i></span>
					<ul>
						<li><a href="home-default.html">Home Default</a></li>
						<li><a href="home-agency.html">Home Agency</a></li>
						<li><a href="home-corporate.html">Home Corporate</a></li>
						<li><a href="home-corporate-2.html">Home Corporate 2</a></li>
						<li><a href="home-slider.html">Home Slider</a></li>
						<li><a href="home-slider-2.html">Home Slider 2</a></li>
						<li><a href="home-video.html">Home Video</a></li>
						<li><a href="home-text-rotator.html">Home Text Rotator</a></li>
						<li><a href="home-shopping.html">Home Shopping</a></li>
						<li><a href="home-border.html">Home Border</a></li>
						<li><a href="home-border-black.html">Home Border Black</a></li>
						<li><a href="home-border-grey.html">Home Border Grey</a></li>
					</ul>
				</li>
				<li><a href="#about">About</a></li>
				<li>
					<a href="#portfolio">Portfolio</a>
					<span class="submenu-trigger"><i class="icon-arrow-down5"></i></span>
					<ul>
						<li><a href="portfolio-2-col.html">Portfolio 2 Col</a></li>
						<li><a href="portfolio-3-col.html">Portfolio 3 Col</a></li>
						<li><a href="portfolio-full-4-col.html">Portfolio 4 Col</a></li>
						<li><a href="portfolio-full-5-col.html">Portfolio 5 Col</a></li>
						<li><a href="portfolio-style-2-2-col.html">Portfolio 2 Col</a></li>
						<li><a href="portfolio-style-2-3-col.html">Portfolio 3 Col</a></li>
						<li><a href="portfolio-style-2-4-col.html">Portfolio 4 Col</a></li>
						<li><a href="portfolio-style-2-5-col.html">Portfolio 5 Col</a></li>
						<li><a href="portfolio-style-3-2-col.html">Portfolio 2 Col</a></li>
						<li><a href="portfolio-style-3-3-col.html">Portfolio 3 Col</a></li>
						<li><a href="portfolio-style-3-4-col.html">Portfolio 4 Col</a></li>
						<li><a href="portfolio-style-3-5-col.html">Portfolio 5 Col</a></li>
						<li><a href="portfolio-single.html">Portfolio Single</a></li>
						<li><a href="portfolio-single-two.html">Portfolio Single Two</a></li>
						<li><a href="portfolio-single-three.html">Portfolio Three</a></li>
						<li><a href="portfolio-single-four.html">Portfolio Single Four</a></li>
						<li><a href="portfolio-single-five.html">Portfolio Single Five</a></li>
						<li><a href="portfolio-single-video.html">Portfolio Single Video</a></li>
						<li><a href="portfolio-single-gallrey.html">Portfolio Single Gallrey</a></li>
						<li><a href="portfolio-single-gallrey-2.html">Portfolio Single Gallrey 2</a></li>
					</ul>
				</li>
				<li><a href="#team">Team</a></li>
				<li class="selected">
					<a href="#blog">Blog</a>
					<span class="submenu-trigger"><i class="icon-arrow-down5"></i></span>
					<ul>
						<li><a href="blog.html">Blog Standard</a></li>
						<li><a href="blog-sidebar.html">Blog with Sidebar</a></li>
						<li><a href="blog-cover.html">Blog with Cover</a></li>
						<li><a href="blog-masonry.html">Blog Masonry</a></li>
						<li><a href="blog-single-page-standard.html">Blog single page</a></li>
						<li><a href="blog-single-page.html">Blog single image page</a></li>
						<li><a href="blog-single-page-quote.html">Blog single page quote</a></li>
						<li><a href="blog-single-page-gallrey.html">Blog single page gallrey</a></li>
						<li><a href="blog-single-page-audio.html">Blog single page audio</a></li>
						<li><a href="blog-single-page-video.html">Blog single page video</a></li>
					</ul>
				</li>
				<li>
					<a href="#blog">Shop</a>
					<ul>
						<li><a href="shop-2-col-sidebar.html">Shop 2 col sidebar</a></li>
						<li><a href="shop.html">Shop 3 col</a></li>
						<li><a href="shop-3-col-sidebar.html">Shop 3 col sidebar</a></li>
						<li><a href="shop-4-col.html">Shop 4 col</a></li>
						<li><a href="shop-single.html">Shop single</a></li>
						<li><a href="shop-cart.html">Shop cart</a></li>
					</ul>
				</li>
				<li><a href="#contact">Contact</a></li>
				<li><a href="shortcodes.html">Shortcodes</a></li>
			</ul>
			<div class="tool-nav-resp">
				<button class="tool-nav-item search-btn-resp"><i class="icon-search2"></i></button>
				<a href="shop-cart.html" class="tool-nav-item"><i class="icon-cart"></i> <span class="cart-resp-count">8</span></a>
			</div>
		</nav>
	</header>
	<!-- *********************
		INTRO PAGE TITLE
	********************** -->
	