<?php get_header(); ?>
	
	<section id="blog" class="content blog no-heading">
		<div class="container">
			<div class="blog-article-container  clearfix col-md-9">
				<!-- BLOG ARTICLE -->
				<?php while(have_posts()): the_post();  ?>
				<article class="blog-article  blog-article-single">
					<div class="blog-article-img media-video blog-article-audio article-quote">
						<?php 
							$video = get_post_meta(get_the_id(), '_for-video', true);
							$audio = get_post_meta(get_the_id(), '_for-audio', true);
							if(!empty($video)){

								echo wp_oembed_get($video);
							}elseif(!empty($audio)){
								echo wp_oembed_get($audio);
							}else(
								the_post_thumbnail()
							)
							 
							
						?>
					</div>
					<div class="blog-article-content">
						<!-- BLOG ARTICLE HEADER -->
						<div class="blog-article-header media">
							<span class="blog-article-media-type cl">
								<i class="icon-camera2"></i>
							</span>
							<div class="bd">
								<h2 class="blog-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="blog-article-info">
									<span class="blog-article-date">
										Posted at: 
										<a href="#"><?php the_time('d M y') ?></a>
									</span>
									<span class="blog-article-tags">
										Posted in: 
										<?php the_tags(); ?>
									</span>
								</div>
							</div>
						</div><!-- BLOG ARTICLE HEADER -->
						<?php the_content(); ?>
						<!-- BLOG ARTICLE FOOTER -->
						<div class="blog-article-footer">
							<div class="blog-article-tags">
								<h5 class="blog-article-tags-title">Tags :</h5>
								
								<?php 
									if(get_the_tag_list()){
										echo get_the_tag_list('<ul><li>', '</li> <li>', '</li><ul>');
									}
								?>

								
							</div>
						</div><!-- BLOG ARTICLE FOOTER -->
					</div><!-- BLOG ARTICLE CONTENT -->
				</article> 
				<?php endwhile; ?>
				<!-- BLOG CONTROL -->
				
				
				<div class="blog-articles-control">
				
					<?php 
						
						the_post_navigation(array(
							'prev_text' => '<span class="ctrl-article-prev">' .__( '&leftarrow; Previous Article', 'rose' ) .
							'</span> ',

							'next_text' => '<span class="ctrl-article-next">' . __( 'Next Article &rightarrow;', 'rose' ) .'</span> ',
								'screen_reader_text' => ' '
						));
						
					?>
				</div>

				<!-- AUTHOR INFO -->
				<div class="blog-author-info">
					<div class="author-info-avatar">
						<?php 
							$author_bio_avatar_size = apply_filters('rose_author_bio_avatar_size',70);
							echo get_avatar(get_the_author_meta('user_email'),
							$author_bio_avatar_size);
						?>

						
					</div>
					<div class="author-info-content">
						<h3 class="author-name"><?php echo get_the_author(); ?></h3>
						<span class="author-function">Web Designer</span>
						
						<?php if(get_the_author_meta('description')) : ?>
							<p>
								<?php the_author_meta( 'description' ); ?>
							</p>
						<?php endif; ?>
					</div>
				</div><!-- author info -->
				
				<?php comments_template(); ?>
				 
				

			</div><!-- BLOG ARTICLE CONTAINER -->

			<!-- BLOG SIDEBAR -->
			<div class="blog-sidebar col-md-3">
				<!-- Sidebar widgets -->
				<ul class="sidebar-widgets">
					<!-- sidebar widget -->
					
						
					
					<?php dynamic_sidebar('right-sidebar'); ?>
				</ul>
			</div><!-- BLOG SIDEBAR -->
		</div>
	</section>


<?php get_footer(); ?>