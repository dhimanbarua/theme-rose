<?php 


/**
* 
*/
class rose_rightside_widget extends WP_Widget {
	
	public function __construct(){ 
		parent:: __construct('rose-latest-post', 'Rose Latest Post',array(
			'description' => 'Custome Latest Post Widget by Rose Theme'
		));
	}
	public function widget($instance, $count){ ?>
		<?php echo $instance['before_widget']; ?>
			<?php echo $instance['before_title']; ?>Latest Posts<?php echo $instance['after_title']; ?>
				<?php 
					$posts = new WP_Query(array(
						'post_type'		=> 'post',
						'posts_per_page' => $count['post_count']
					));
				?>
			<ul>
				<?php while($posts->have_posts()) : $posts->the_post(); ?>
				<li class="media">
					<div class="cl">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					</div>
					<div class="bd">
						<h4 class="latest-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<span class="post-time">
							<?php if(!empty($count['date'])): ?>
								<a href="#"><?php the_time('d/m/Y'); ?></a>
							<?php endif; ?>
						</span>
					</div>
				</li>
				<?php endwhile; ?>
			</ul>
		<?php echo $instance['after_widget']; ?>
	<?php }

	public function form($val){ ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title: </label>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $val['title']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>">Number of posts to show:</label>
			<input class="tiny-text" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>" step="1" min="1" value="<?php echo $val['post_count']; ?>" size="3" type="number">
		</p>
		<p>
			<input type="checkbox" id="<?php echo $this->get_field_id('date'); ?>" name="<?php echo $this->get_field_name('date'); ?>" value="showdate" <?php if(!empty($val['date'])){echo "checked='checked'";} ?>>
			<label for="<?php echo $this->get_field_id('date'); ?>">Display post date?</label>
		</p>
		
	<?php }

}
add_action('widgets_init', 'latest_post_widget');
	function latest_post_widget(){
		register_widget('rose_rightside_widget');
	}

class rose_footer_widget extends WP_Widget {
	public function __construct(){
		parent::__construct('footer-latest-post', 'Footer Latest Post',array(
			'description' => 'Custome Footer Latest Post Widget by Rose Theme'
		));
	}

	public function widget($instance, $count){ ?>
		<?php echo $instance['before_widget']; ?>
			<?php echo $instance['before_title']; ?>Latest Posts<?php echo $instance['after_title']; ?>
			<?php 
				$footerpost = new WP_Query(array(
					'post_type' 	=> 'post',
					'posts_per_page'	=>	$count['post_count']
				));
			?>
			<ul class="">
				<?php while($footerpost->have_posts()) : $footerpost->the_post(); ?>
				<li>
					<h4 class="widget-blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<span class="widget-blog-author"><a href="#"><?php the_author(); ?></a></span>
				</li>
				<?php endwhile; ?>
			</ul>
		<?php echo $instance['after_widget']; ?>
	<?php }

	public function form($val){ ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title: </label>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $val['title']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('post_count'); ?>">Number of posts to show:</label>
			<input class="tiny-text" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>" step="1" min="1" value="<?php echo $val['post_count']; ?>" size="3" type="number">
		</p>

	<?php }
}
	add_action('widgets_init', 'footer_latest_post');

	function footer_latest_post(){
		register_widget('rose_footer_widget');
	}
