<?php

/**
 * Rose functions and definitions
 * Anybody can use the theme but he/she will have to maintain 	the GPL 2 licence
 * Here you will get all the functions of Comet Fifteen
 */

 // CMB2 Box(Meta Box)
 if(file_exists(dirname(__FILE__).'/lib/metabox/init.php')){
	require_once(dirname(__FILE__).'/lib/metabox/init.php');
 }
 if(file_exists(dirname(__FILE__)).'/lib/metabox/config.php'){
	require_once(dirname(__FILE__).'/lib/metabox/config.php');
 }
 // Custome right sidebar widget
 if(file_exists(dirname(__FILE__).'/widgets.php')){
 	require_once(dirname(__FILE__).'/widgets.php');
 }
 // ReduxCore FrameWork
 if(file_exists(dirname(__FILE__).'/lib/ReduxCore/framework.php')){
	require_once(dirname(__FILE__).'/lib/ReduxCore/framework.php');
 }
 if(file_exists(dirname(__FILE__).'/lib/sample/custom-config.php')){
	require_once(dirname(__FILE__).'/lib/sample/custom-config.php');
 }
 // Custom Shortcode
 if(file_exists(dirname(__FILE__).'/shortcodes/shortcodes.php')){
 	require_once(dirname(__FILE__).'/shortcodes/shortcodes.php');
 }
// Custom Shortcode
 if(file_exists(dirname(__FILE__).'/custom-nav-walker.php')){
 	require_once(dirname(__FILE__).'/custom-nav-walker.php');
 }
// plugins activation 
if(file_exists(dirname(__FILE__).'/lib/plugins/required-plugins.php')){
 	require_once(dirname(__FILE__).'/lib/plugins/required-plugins.php');
 }


 // theme setup function
 add_action('after_setup_theme', 'rose_functions');
/**
 *
 */
function rose_functions(){
  	// Text domain
  	load_theme_textdomain('rose', get_template_directory().'/lang');

  	// theme supports
  	add_theme_support('title-tag');
  	add_theme_support('post-thumbnails');
  	add_theme_support('post-formats', array(
  		'video',
  		'audio',
  		'standard',
  		'gallery',
  		'quote'
  	));

	// Main Menu
	register_nav_menu('main-menu', __('Main Menu', 'rose'));

	// rose slider
	register_post_type('rose-slider', array(
		'labels' 	=> array(
			'name'			=> __('Slider', 'rose'),
			'add_new'		=> __('Add New Slider', 'rose'),
			'add_new_item'	=> __('Add New Slider', 'rose')
		),
		'public'		=> true,
		'supports'		=> array('title', 'editor', 'thumbnail'),
		'show_in_menu'	=> 'front-sections',	
	));

	// filtering portfolio
	if(current_user_can('manage_options')){
		register_post_type('rose-portfolio', array(
			'labels'		=> array(
				'name'			=> __('Portfolio', 'rose'),
				'add_new'		=> __('Add New Portfolio', 'rose'),
				'add_new_item'	=> __('Add New Portfolio', 'rose')
			),
			'public'		=> true,
			'supports'		=> array('title', 'editor', 'thumbnail'),
			'show_in_menu'	=> 'front-sections',
		));
	}
	register_taxonomy('rose-portfolio-category', 'rose-portfolio', array(
		'labels'		=> array(
			'name'			=> __('Types', 'rose'),
			'add_new'		=> __('Add New Category', 'rose'),
			'add_new_item'	=> __('Add New Category', 'rose')
		),
		'public'		=> true,
		'hierarchical'	=> true
	));
	// Rose BRANDS
	if(current_user_can('manage_options')){
		register_post_type('rose-brands', array(
			'labels'	=> array(
				'name'			=> __('Brands', 'rose'),
				'add_new'		=> __('Add New Brand', 'rose'),
				'add_new_item'	=> __('Add New Brand', 'rose')
			),
			'public'		=> true,
			'supports'		=> array('thumbnail'),
			'show_in_menu'	=>	'front-sections'
		));
	}
	// Rose Client Comment
	if(current_user_can('manage_options')){
		register_post_type('rose-clients', array(
			'labels'	=> array(
				'name'			=> __('Clients', 'rose'),
				'add_new'		=> __('Add New Client', 'rose'),
				'add_new_item'	=> __('Add New Client', 'rose'),
			),
			'public'			=> true,
			'supports'			=> array('editor'),
			'show_in_menu'		=> 'front-sections'
		));
	}
	// Rose Team Member
	if(current_user_can('manage_options')){
		register_post_type('rose-team', array(
			'labels'	=> array(
				'name'			=> __('Team', 'rose'),
				'add_new'		=> __('Add New Team', 'rose'),
				'add_new_item'	=> __('Add New Team', 'rose'),
			),
			'public'			=> true,
			'supports'			=> array('title', 'editor', 'thumbnail'),
			'show_in_menu'		=> 'front-sections'
		));
	}
	// Rose Services
	if(current_user_can('manage_options')){
		register_post_type('rose-services', array(
			'labels'		=> array(
				'name'			=> __('service', 'rose'),
				'add_new'		=> __('Add New Service', 'rose'),
				'add_new_item'	=> __('Add New Service', 'rose'),
			),
			'public'		=> true,
			'supports'		=> array('title', 'editor'),
			'show_in_menu'	=> 'front-sections'
		));
	}

/*
*	Front Section
*/
add_action('admin_menu', 'frontpage_admin_menu');
	function frontpage_admin_menu(){
		add_menu_page(
			'Front Sections',
			'Front Sections',
			'read',
			'front-sections',
			'',
			'dashicons-admin-home',
			2
		);
	}
	
 
 
 }
 // Adding fonts
  function get_rose_fonts(){
  	$fonts = array();
  	$fonts[] = 'Open+Sans:300italic,400italic,700italic,400,300,700';
  	$fonts[] = 'Oswald:400,300,700';
  	$rose_fonts = add_query_arg(array(
  		'family' => urlencode(implode('|', $fonts)),
  		'subset' => 'latin'
  	), 'http://fonts.googleapis.com/css');

  	return $rose_fonts;
   }
 // including theme styles
add_action('wp_enqueue_scripts', 'rose_styles');
   function rose_styles(){
  	wp_enqueue_style('bootstrap', get_template_directory_uri().'/css/bootstrap.css');
  	wp_enqueue_style('animate', get_template_directory_uri().'/css/animate.css');

  	wp_enqueue_style('settings', get_template_directory_uri().'/rs-plugin/css/settings.css');
  	wp_enqueue_style('style', get_template_directory_uri().'/css/style.css');
  	wp_enqueue_style('color', get_template_directory_uri().'/css/color.css');
  	wp_enqueue_style('javascript', get_template_directory_uri().'/css/javascript.css');
  	wp_enqueue_style('stylesheet', get_stylesheet_uri());
  	wp_enqueue_style('responsive', get_template_directory_uri().'/css/responsive.css');
  	wp_enqueue_style('fonts', get_rose_fonts());
   }

 // including theme scripts
 add_action('wp_enqueue_scripts', 'rose_scripts');
 function rose_scripts(){
	wp_enqueue_script('flexslider', get_template_directory_uri().'/js/jquery.flexslider.js', array('jquery'), '', true);

	wp_enqueue_script('parallax', get_template_directory_uri().'/js/jquery.parallax-1.1.3.js', array('jquery'), '', true);

	wp_enqueue_script('scrollto', get_template_directory_uri().'/js/jquery.scrollto-1.4.3.1.js', array('jquery'), '', true);

	wp_enqueue_script('localscroll', get_template_directory_uri().'/js/jquery.localscroll-1.2.7.js', array('jquery'), '', true);

	wp_enqueue_script('isotope', get_template_directory_uri().'/js/jquery.isotope.js', array('jquery'), '', true);

	wp_enqueue_script('masonry', get_template_directory_uri().'/js/jquery.masonry.min.js', array('jquery'), '', true);

	wp_enqueue_script('carousel', get_template_directory_uri().'/js/owl.carousel.js', array('jquery'), '', true);

	wp_enqueue_script('countTo', get_template_directory_uri().'/js/jquery.countTo.js', array('jquery'), '', true);

	wp_enqueue_script('wow', get_template_directory_uri().'/js/wow.js', array('jquery'), '', true);

	wp_enqueue_script('tweetie', get_template_directory_uri().'/js/tweetie.min.js', array('jquery'), '', true);

	wp_enqueue_script('tabby', get_template_directory_uri().'/js/tabby.min.js', array('jquery'), '', true);

	wp_enqueue_script('instafeed', get_template_directory_uri().'/js/instafeed.min.js', array('jquery'), '', true);

	wp_enqueue_script('magnific', get_template_directory_uri().'/js/jquery.magnific-popup.js', array('jquery'), '', true);

	wp_enqueue_script('rotator', get_template_directory_uri().'/js/jquery.simple-text-rotator.js', array('jquery'), '', true);

	wp_enqueue_script('smoothscroll', get_template_directory_uri().'/js/jquery.smoothscroll.js', array('jquery'), '', true);

	wp_enqueue_script('validator', get_template_directory_uri().'/js/validator.min.js', array('jquery'), '', true);

	wp_enqueue_script('themepunch-tools', get_template_directory_uri().'/rs-plugin/js/jquery.themepunch.tools.min.js', array('jquery'), '', true);

	wp_enqueue_script('themepunch-revolution', get_template_directory_uri().'/rs-plugin/js/jquery.themepunch.revolution.min.js', array('jquery'), '', true);

	wp_enqueue_script('functions', get_template_directory_uri().'/js/functions.js', array('jquery'), '', true);
}

 // post format
 add_action('admin_print_scripts', 'rose_sp_scripts', 1000);
 function rose_sp_scripts(){ ?>

 	<?php if(get_post_type() == 'post') : ?>

	 	<script>
	 		jQuery(document).ready(function(){

	 	var id = jQuery("input[name='post_format']:checked").attr('id');

	 		if(id == 'post-format-video'){
	 			jQuery('.cmb2-id--for-video').show();
	 		}else{
	 			jQuery('.cmb2-id--for-video').hide();
	 		}

	 		if(id == 'post-format-audio'){
	 			jQuery('.cmb2-id--for-audio').show();
	 		}else{
	 			jQuery('.cmb2-id--for-audio').hide();
	 		}

	 		if(id == 'post-format-gallery'){
	 			jQuery('.cmb2-id--for-gallery').show();
	 		}else{
	 			jQuery('.cmb2-id--for-gallery').hide();
	 		}


	 		jQuery("input[name='post_format']").change(function(){
	 			jQuery('.cmb2-id--for-video').hide();
	 			jQuery('.cmb2-id--for-audio').hide();
	 			jQuery('.cmb2-id--for-gallery').hide();
	 		var id = jQuery("input[name='post_format']:checked").attr('id');

	 			if(id == 'post-format-video'){
	 				jQuery('.cmb2-id--for-video').show();
	 			}else{
	 				jQuery('.cmb2-id--for-video').hide();
	 			}

	 			if(id == 'post-format-audio'){
	 				jQuery('.cmb2-id--for-audio').show();
	 			}else{
	 				jQuery('.cmb2-id--for-audio').hide();
	 			}

	 			if(id == 'post-format-gallery'){
	 				jQuery('.cmb2-id--for-gallery').show();
	 			}else{
	 				jQuery('.cmb2-id--for-gallery').hide();
	 			}
			});
	 	})
	 	</script>
	<?php endif; ?>
<?php }

 // sidebar widgets
add_action('widgets_init', 'rose_rightsidebar');
function rose_rightsidebar(){
	register_sidebar(array(
		'name'			=> __('Right sidebar', 'rose'),
		'description'	=> __('You may add your right sidebar widgets here', 'rose'),
		'id'			=> 'right-sidebar',
		'before_widget'	=> '<li class="sidebar-widget">',
		'after_widget'	=> '</li>',
		'before_title'	=> '<h3 class="sidebar-widget-title">',
		'after_title'	=> '</h3>'
	));

	register_sidebar(array(
		'name'			=>	__('Footer Widgets', 'rose'),
		'description'	=>  __('You may add your footer widgets here', 'rose'),
		'id'			=> 'footer-widgets',
		'before_widget'	=> '<div class="footer-widget widget-blog col-md-3">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="widget-title">',
		'after_title'	=> '</h3>'
	));
}

/* 
*	flush rewrite rules
*/
register_activation_hook(__FILE__, 'flush_rewrite');
	function flush_rewrite(){
		flush_rewrite_rules();
	}

/* 
*	Custome Comment
*/

add_filter('comment_form_default_fields', 'rose_comment_form');
	function rose_comment_form($default){
			$default['author'] = '<div class="triple-input">
							<div class="input-col col-1">
								<input type="text" id="name" name="author" placeholder="Full name">
							</div>';
			$default['email'] = '<div class="input-col col-1">
								<input type="text" id="mail" name="email" placeholder="E-mail">
							</div>';
			$default['url'] = '<div class="input-col col-1">
								<input type="text" id="website" name="url" placeholder="Website">
							</div></div>';
			$default['comment_box'] = '<textarea name="comment" id="comment" placeholder="Write your comment here..."></textarea>';

			return $default;
	}
add_filter('comment_form_defaults', 'rose_defaults_comment_form');
	function rose_defaults_comment_form($default_info){

		if(!is_user_logged_in()){
			$default_info['comment_field'] = '';
		}else{
			$default_info['comment_field'] = '<textarea name="comment" id="comment" placeholder="Write your comment here..."></textarea>';
		}


		$default_info['submit_button'] = '<button class="m-btn m-btn-medium m-button-default">Submit</button>';
		$default_info['comment_notes_before'] = '';
		$default_info['title_reply'] = 'Leave a comment';

		return $default_info;
	}

	function my_comments_callback($comment, $arg, $depth){

		$GLOBALS['comment'] = $comment;
		?>
			<li class="comment">
				<div class="comment-author">
					<?php 
						echo get_avatar($comment, '', '', '', array(
							'class' => ''
						));
					?>
				</div>
				<div class="comment-content">
					<h4 class="author-name">
						<?php 
							echo get_comment_author_link();
						?>
					</h4>
					<span class="comment-time"><?php comment_date('F j, Y'); ?></span>
					<p>
						<?php 
							comment_text();
						?>
					</p>
					<span class="reply">
						<?php 
							comment_reply_link(
                				array_merge($arg, array(
	            					'depth' 		=> $depth,
	            					'max_depth'	=> $arg['max_depth']
								))
                			);
						?>
					</span>
				</div>
			</li>

		<?php

	}
//login page custom logo

add_action("login_enqueue_scripts", "my_login_logo");
	function my_login_logo(){ ?>
	    <style type="text/css">
	        #login h1 a, .login h1 a{
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
	            background-size: 200px 150px;
	            width: 300px;
	            height: 145px;
	            line-height: 1.3em;
	        }
	    </style>
	<?php }

//login page custom logo title

add_filter("login_headertitle", "my_login_logo_title");
	function my_login_logo_title(){
	    return "Powerd by Rose";
	}

//login page custom logo URL

add_filter("login_headerurl", "my_login_logo_url");
	function my_login_logo_url(){
	    return "http://www.rose.com";
	}

// visual composer custome widget
if(function_exists('vc_map')):
add_action('vc_before_init', 'set_as_theme_vc');
	function set_as_theme_vc(){
		vc_set_as_theme();
	}

vc_map(array(
	'name'						=> 'rose slider',
	'base'						=> 'rose-slider',
	'show_settings_on_create'	=> false
));
vc_map(array(
	'name'		=> 'rose about',
	'base'		=> 'about-section',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'Rose for your needs',
		),
		array(
			'type'		=> 'textarea_html',
			'heading'	=> 'Description',
			'param_name'=> 'content',
			'value'		=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat.'
		),
		array(
			'type'		=> 'attach_image',
			'heading'	=> 'About Section Images Imac',
			'param_name'=> 'bgimage_big',
			
		),
		array(
			'type'		=> 'attach_image',
			'heading'	=> 'About Section Iphone',
			'param_name'=> 'bgimage_medium',
			
		),
		array(
			'type'		=> 'attach_image',
			'heading'	=> 'About Section Images Iwatch',
			'param_name'=> 'bgimage_small',
			
		),
	)
));
vc_map(array(
	'name'		=> 'Rose Counter',
	'base'		=> 'rose-counter',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'First Icon',
			'param_name'=> 'first_icon'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'First Data',
			'param_name'=> 'first_data'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'First Title',
			'param_name'=> 'first_title'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Second Icon',
			'param_name'=> 'second_icon'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Second Data',
			'param_name'=> 'second_data'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Second Title',
			'param_name'=> 'second_title'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Third Icon',
			'param_name'=> 'third_icon'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Third Data',
			'param_name'=> 'third_data'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Third Title',
			'param_name'=> 'third_title'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Fourth Icon',
			'param_name'=> 'fourth_icon'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Fourth Data',
			'param_name'=> 'fourth_data'
		),
		array(
			'type'		=> 'textfield',
			'heading' 	=> 'Fourth Title',
			'param_name'=> 'fourth_title'
		),

	)
));
vc_map(array(
	'name'		=> 'Rose Portfolio',
	'base'		=> 'rose-portfolio',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'Portfolio'
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Subtitle',
			'param_name'=> 'subtitle',
			'value'		=> 'display our latest works'
		)
	)
));
vc_map(array(
	'name'		=> 'Rose quote',
	'base'		=> 'rose-quote',
	'show_settings_on_create'	=> false
));
vc_map(array(
	'name'		=> 'Rose Team',
	'base'		=> 'rose-team',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'The Team'
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Subtitle',
			'param_name'=> 'subtitle',
			'value'		=> 'we are talented people'
		),

	)
));
vc_map(array(
	'name'		=> 'Rose Services',
	'base'		=> 'rose-service',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'What we do',
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Subtitle',
			'param_name'=> 'subtitle',
			'value'		=> 'Check out our services talents',
		)
	)
));
vc_map(array(
	'name'		=> 'Rose Banner',
	'base'		=> 'rose-banner',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'LOOKING FOR EXCLUSIVE DIGITAL SERVICES?',
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Add Button Text',
			'param_name'=> 'button',
			'value'		=> 'Lets Talk About',
		),
		array(
			'type'		=> 'textarea_html',
			'heading'	=> 'Description',
			'param_name'=> 'content',
			'value'		=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.',
		)
	)
));
vc_map(array(
	'name'		=> 'Rose Blog',
	'base'		=> 'rose-blog',
	'params'		=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'The Blog',
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Subtitle',
			'param_name'=> 'subtitle',
			'value'		=> 'They are all happy',
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Add Blog Link',
			'param_name'=> 'link',
			'value'		=> 'http://localhost/themerose/index.php/blog/',
		),

	)
));
vc_map(array(
	'name'						=> 'Rose Brand',
	'base'						=> 'rose-brands',
	'show_settings_on_create'	=> false
));
vc_map(array(
	'name'		=> 'Rose Contact',
	'base'		=> 'rose-contact',
	'params'	=> array(
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Title',
			'param_name'=> 'title',
			'value'		=> 'Contact Us',
		),
		array(
			'type'		=> 'textfield',
			'heading'	=> 'Subtitle',
			'param_name'=> 'subtitle',
			'value'		=> 'Drop us some lines.',
		),
		array(
			'type'		=> 'textarea_html',
			'heading'	=> 'Add Description',
			'param_name'=> 'content',
			'value'		=> 'Lorem ipsum ex vix illud nonummy, novum tation et his. At vix scriptaset patrioque scribentur, at pro fugit erts verterem molestiae.',
		),
	)
));
endif;

