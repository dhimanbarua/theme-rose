<?php 



?>

<div class="blog-article-comments">
	<h3 class="article-comments-count">This article has <?php comments_number(); ?></h3>
	<ul class="blog-article-comments-list">
<?php

	wp_list_comments(array(
		'callback'	=> 'my_comments_callback'
	));
	
?>
	</ul>
</div>
<?php
comment_form();