<?php 


add_shortcode('rose-slider', 'rose_home_slider');

	function rose_home_slider(){

		ob_start(); ?>

			<section class="intro full-height align-center" id="intro">
			<div class="fullwidthbanner-container">
				<div class="banner">
					<ul class="container">
						<!-- THE BOXSLIDE EFFECT EXAMPLES  WITH LINK ON THE MAIN SLIDE EXAMPLE -->
						<?php 
							$sliders = new WP_Query(array(
								'post_type'			=> 'rose-slider',
								'posts_per_page'	=> 3
							));

							while($sliders->have_posts()) : $sliders->the_post(); 
						?>
						<li data-transition="slotfade-horizontal" data-slotamount="7">
						   	<?php the_post_thumbnail(); ?>
						   	<h1 class="caption sft intro-huge-title upper"  data-x="center" data-y="200" data-speed="700" data-start="1700" data-easing="easeOutBack"><?php the_title(); ?></h1>
						   	<div class="caption sfb intro-sub-title"  data-x="center" data-y="340" data-speed="500" data-start="1900" data-easing="easeOutBack"><?php echo get_post_meta(get_the_id(), '_slider-subtitle', true); ?></div>
						   	<div class="caption sfb"  data-x="center" data-y="420" data-speed="320" data-start="2000">
						   		<?php 
						   			$button = get_post_meta(get_the_id(), '_first-button-text', true);

						   			if(!empty($button)) :
						   		?>
						   		<a href="<?php the_permalink(); ?>" class="m-btn m-btn-default m-btn-radius m-btn-large"><?php echo $button; ?></a>

						   		<?php endif; ?>
						   	</div>
						</li>
						
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
		</section>

		<?php return ob_get_clean();
		
	}
/*
*	Theme Rose About Section
*/
add_shortcode('about-section', 'rose_about_section');
	function rose_about_section($attr, $content=NULL){
		$attribute = extract(shortcode_atts(array(
			'title'			=> 'Rose for your needs',
			'bgimage_big'	=> get_template_directory_uri().'/images/media/imac.png',
			'bgimage_medium'=> get_template_directory_uri().'/images/media/iphone.png',
			'bgimage_small'	=> get_template_directory_uri().'/images/media/iwatch.png',
		), $attr));
		ob_start(); ?>
			<section class="about section-area" id="about">
			<div class="container">
				<div class="col-md-6">
					<div class="animated-box">
						<img src="<?php echo wp_get_attachment_image_url($bgimage_big, 'large'); ?>" class="scale wow fadeInUp" data-wow-duration=".8s" data-wow-delay=".8s" alt="">
						<img src="<?php echo wp_get_attachment_image_url($bgimage_medium, 'large'); ?>" class="scale animated-img wow fadeIn" data-wow-duration="1.2s" data-wow-delay="1s" alt="">
						<img src="<?php echo wp_get_attachment_image_url($bgimage_small, 'large'); ?>" class="scale animated-img wow fadeIn" data-wow-duration="1.4s" data-wow-delay="1s" alt="">
					</div>
				</div>
				<div class="col-md-5 col-md-offset-1 wow fadeInRight" data-wow-duration="1.2s" data-wow-delay=".8s">
					<h1 class="upper"><?php echo $title; ?></h1>
					<p class="text-medium"><?php echo do_shortcode($content); ?></p>
					<a href="#">
						<span class="m-btn  m-btn-default m-btn-large m-btn-radius">Purchase Now</span>
					</a>
				</div>
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose Counter Section
*/
add_shortcode('rose-counter', 'rose_counter_section');
	function rose_counter_section($attr, $content=NULL){
		$CounterAttr = extract(shortcode_atts(array(
			'first_icon'	=> 'et-icon-pencil',
			'first_data'	=> '968',
			'first_title'	=> 'Lines of HTML',
			'second_icon'	=> 'et-icon-megaphone',
			'second_data'	=> '200',
			'second_title'	=> 'Cups of coffee',
			'third_icon'	=> 'et-icon-map',
			'third_data'	=> '115',
			'third_title'	=> 'finished projects',
			'fourth_icon'	=> 'et-icon-layers',
			'fourth_data'	=> '90',
			'fourth_title'	=> 'startups'

		), $attr));
		ob_start(); ?>
			<section class="stats grey-bg">
			<div class="container no-pd">
				<!-- COUNTER ITEM -->
				<div class="counter-item col-md-3 col-xs-6">
					<div class="wow fadeInUp" data-wow-delay="0.7s">
						<i class="counter-item-icon <?php echo $first_icon; ?>"></i>
						<span class="timer default-color" data-to="<?php echo $first_data; ?>" data-speed="10000"></span>
						<span class="counter-item-text"><?php echo $first_title; ?></span>
					</div>
				</div>
				<!-- COUNTER ITEM -->
				<div class="counter-item col-md-3 col-xs-6">
					<div class="wow fadeInUp" data-wow-delay="0.7s">
						<i class="counter-item-icon <?php echo $second_icon; ?>"></i>
						<span class="timer default-color" data-to="<?php echo $second_data; ?>" data-speed="10000"></span>
						<span class="counter-item-text"><?php echo $second_title; ?></span>
					</div>
				</div>
				<!-- COUNTER ITEM -->
				<div class="counter-item col-md-3 col-xs-6">
					<div class="wow fadeInUp" data-wow-delay="0.7s">
						<i class="counter-item-icon <?php echo $third_icon; ?>"></i>
						<span class="timer default-color" data-to="<?php echo $third_data; ?>" data-speed="10000"></span>
						<span class="counter-item-text"><?php echo $third_title; ?></span>
					</div>
				</div>
				<!-- COUNTER ITEM -->
				<div class="counter-item col-md-3 col-xs-6">
					<div class="wow fadeInUp" data-wow-delay="0.7s">
						<i class="counter-item-icon <?php echo $fourth_icon; ?>"></i>
						<span class="timer default-color" data-to="<?php echo $fourth_data; ?>" data-speed="10000"></span>
						<span class="counter-item-text"><?php echo $fourth_title; ?></span>
					</div>
				</div>
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose Portfolio Section
*/

add_shortcode('rose-portfolio', 'rose_portfolio_section');
	function rose_portfolio_section($attr, $content=null){

		$filter = extract(shortcode_atts(array(
			'title'		=> "Portfolio",
			'subtitle'	=>	"display our latest works"
		), $attr));
		ob_start(); ?>
			<section class="portfolio portfolio-style-two section-area no-pd-b" id="portfolio">
			<div class="container-fluid no-pd">
				<div class="heading-wrap col-md-12">
					<h2 class="section-title"><?php echo $title; ?></h2>
					<span class="section-subtitle-m section-subtitle-color"><?php echo $subtitle; ?></span>
				</div>
				<!-- PORTFOLIO CONTAINER -->
				<div class="portfolio-container">
					<?php 
						$portfolio = new WP_Query(array(
							'post_type' => 'rose-portfolio',
							'posts_per_page'	=> 8
						));

						while($portfolio->have_posts()): $portfolio->the_post();
					?>
					
					<div class="portfolio-item <?php 
											$terms = get_the_terms(get_the_id(), 'rose-portfolio-category');
											foreach($terms as $term ){
												echo $term->slug." ";
											}
										?>">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail(); ?>
							<div class="portfolio-item-caption">
								<h2 class="portfolio-item-title"><?php the_title(); ?></h2>
								<div class="portfolio-item-cats">
									<span>
										<?php 
											$terms = get_the_terms(get_the_id(), 'rose-portfolio-category');
											foreach($terms as $term ){
												echo $term->name." ";
											}
										?>
									</span>
									
								</div>
							</div>
						</a>
					</div>
				<?php endwhile; ?>
				</div><!-- END PORTFOLIO CONTAINER -->
			</div>
		</section>

		<?php return ob_get_clean();
	}

/*
*	Theme Rose QUOTE Section
*/
add_shortcode('rose-quote', 'rose_quote_section');
	function rose_quote_section(){
		
		ob_start(); ?>
			<section class="quote-section dark-bg no-pd">
			<div class="container-fluid no-pd">
				<div class="col-md-6 no-pd m-mhw">
					<div class="quote-slider-wrap m-mhw-content">
						<div class="quote-slider-white flex-trans flexslider">
			  				<ul class="slides">
			  					<?php 
			  						$client = new WP_Query(array(
			  							'post_type'	=> 'rose-clients',
			  							'posts_per_page' => 3
			  						));

			  						while($client->have_posts()) : $client->the_post();
			  					?>
					 	 		<li>
									<p><?php the_content(); ?></p>
									<span class="quote-w-author"><?php echo get_post_meta(get_the_id(), '_for-clients', true); ?></span>
								</li>
								<?php endwhile; ?>
								
				  			</ul>
			  			</div><!-- END QUOTE SLIDER -->
					</div>
				</div><!-- mhw -->
				<div class="quote-media col-md-6 m-mhw about no-pd">
					
				</div>
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose TEAM Section
*/
add_shortcode('rose-team', 'rose_team_section');
	function rose_team_section($attr, $content=Null){
		$attribute = extract(shortcode_atts(array(
			'title'		=> 'The Team',
			'subtitle'	=> 'we are talented people'
		), $attr));
		ob_start(); ?>
			<section class="team">
				<div class="container">
					<div class="heading-wrap col-md-12">
						<h2 class="section-title"><?php echo $title; ?></h2>
						<span class="section-subtitle-m section-subtitle-color"><?php echo $subtitle; ?></span>
					</div>
				<div class="m-deso-odd">
					<!-- Team member -->
					<?php 
						$teams = new WP_Query(array(
							'post_type' 		=> 'rose-team',
							'posts_per_page'	=> 3,
						));

						while($teams->have_posts()) : $teams->the_post();
					?>
					<div class="m-team-member wow col-md-4 col-xs-6 fadeInUp" data-wow-delay=".7s">
						<div class="m-team-wrap">
							<?php the_post_thumbnail(); ?>
							<div class="m-team-cap">
								<div class="m-team-member-social-icons clearfix">
								<?php 
									$skypeicon = get_post_meta(get_the_id(), '_for-skypeicon', true);
									$flickricon = get_post_meta(get_the_id(), '_for-flickricon', true);
								?>
									<a href="#"><i class="icon-<?php echo get_post_meta(get_the_id(), '_for-facebookicon', true); ?>"></i></a>
									<a href="#"><i class="icon-<?php echo get_post_meta(get_the_id(), '_for-twittericon', true); ?>"></i></a>
									<a href="#"><i class="icon-<?php if(!empty($skypeicon)){
										echo $skypeicon;
										}
										elseif(!empty($flickricon)){
										echo $flickricon;
											}  ?>"></i></a>
									

								</div>
							</div>
						</div><!-- team wrap -->
						<h3 class="m-team-member-name"><?php echo get_post_meta(get_the_id(), '_for-teamMemberName', true); ?></h3>
						<span class="m-team-member-role"><?php echo get_post_meta(get_the_id(), '_for-teamExpertSkill', true); ?></span>
						<p class="m-team-member-desc">
							<?php the_content(); ?>
						</p>
					</div><!-- End Team member -->
					<?php endwhile; ?>
				</div> <!-- DESO -->
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose Service Section
*/
add_shortcode('rose-service', 'rose_service_section');
	function rose_service_section($attr, $content=Null){
		$serviceAttr = extract(shortcode_atts(array(
			'title'		=> 'What we do',
			'subtitle'	=> 'Check out our services talents',
		), $attr));
		ob_start(); ?>
			<section class="features">
			<div class="container">
				<div class="heading-wrap col-md-12">
					<h1 class="section-title"><?php echo $title ?></h1>
					<span class="section-subtitle-m section-subtitle-color"><?php echo $subtitle; ?></span>
				</div>
				<!-- FEATURE ITEM -->
				<?php 
					$services = new WP_Query(array(
						'post_type'		=> 'rose-services',
						'posts_per_page'=> 9,
					));

					while($services->have_posts()): $services->the_post();
				?>
				<div class="m-features-item clearfix col-md-4 col-xs-6 wow fadeInUp" data-wow-delay="0.7s">
					<div class="m-features-icon"><i class="<?php echo get_post_meta(get_the_id(), '_for-serviceIcon', true); ?>"></i></div>
					<div class="m-features-content">
						<h3 class="m-features-title upper"><?php the_title(); ?></h3>
						<div class="m-features-desc">
							<p><?php the_content(); ?></p>
						</div>
					</div>
				</div><!-- END FEATURE ITEM -->
				<?php endwhile; ?>
				
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose Banner Section
*/
add_shortcode('rose-banner', 'rose_banner_section');
	function rose_banner_section($attr, $content=NULL){
		$bannerAttr = extract(shortcode_atts(array(
			'title'		=> 'LOOKING FOR EXCLUSIVE DIGITAL SERVICES?',
			'button'	=> 'Lets Talk About'
			
		), $attr));
		ob_start(); ?>
		<section class="parallax no-pd cover-bg">
			
			<div class="container">
				<div class="col-md-12 m-mhw">
					<div class="m-mhw-content align-center">
						<h1 class="upper white wow fadeInUp" data-wow-delay=".7s"><?php echo $title; ?></h1>
						<div class="spacer"></div>
						<p class="white wow fadeInUp" data-wow-delay=".9s"><?php echo do_shortcode($content); ?></p>
						<div class="spacer"></div>
						<a href="#contact" class="m-btn m-btn-radius m-btn-white m-btn-medium wow fadeInUp" data-wow-delay="1.1s"><?php echo $button; ?> &rarr;</a>
					</div>
				</div>
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose BLOG Section
*/
add_shortcode('rose-blog', 'rose_blog_section');
	function rose_blog_section($attr, $content=NULL){
		$blogAttr = extract(shortcode_atts(array(
			'title'		=> 'The Blog',
			'subtitle'	=> 'They are all happy',
			'link'		=> 'http://localhost/themerose/index.php/blog/'

		), $attr));
		ob_start();	?>
			<section class="blog section-area" id="blog">
			<div class="container">
				<div class="heading-wrap col-md-12">
					<h2 class="section-title"><?php echo $title; ?></h2>
					<span class="section-subtitle-m"><?php echo $subtitle; ?></span>
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="blog-listed-style">
						<?php 
							$blog = new WP_Query(array(
								'post_type' 		=> 'post',
								'posts_per_page'	=>	4
							));
							while($blog->have_posts()): $blog->the_post();
						?>
						<!--  -->
						<article class="blog-listed-article media wow fadeInUp" data-wow-delay=".8s">
							<a href="<?php the_permalink(); ?>">
								<div class="blog-listed-media cl">
									<?php the_post_thumbnail( 'thumbnail', array(
										'class' => 'blog-article-thumb'
									)); ?>

									<div class="article-date">
										<span><?php the_time('d'); ?></span>
										<?php the_time('M'); ?>
									</div>
								</div>
								<div class="bd">
									<h2 class="blog-listed-title"><?php the_title(); ?></h2>
								</div>
							</a>
						</article>
					<?php endwhile; ?>
						
						
						<div class="spacer"></div><!--  -->
						<div class="spacer"></div><!--  -->
						<div class="col-md-12 align-center">
							<a href="<?php echo $link; ?>" class="m-btn m-btn-large m-btn-border m-btn-radius">Go to the blog</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose BRANDS Section
*/
add_shortcode('rose-brands', 'rose_brands_section');
	function rose_brands_section(){
		
		ob_start(); ?>
			<section class="brands dark-bg">
			<div class="container">
				<ul class="brand-list-carousel col-md-12">
				<?php 
					$brands = new WP_Query(array(
						'post_type' 		=> 'rose-brands',
						'posts_per_page'	=> 8
					));

				while($brands->have_posts()) : $brands->the_post();
				?>
					<?php 

						$brandimgs = get_post_meta(get_the_id(), '_for-brands', true); 
						foreach ($brandimgs as $brandimg ) : ?>
					<li class="brand-item"><img src="<?php echo $brandimg; ?>" alt=""></li>
				<?php endforeach; ?>
					<?php endwhile; ?>
				</ul>
			</div>
		</section>
		<?php return ob_get_clean();
	}
/*
*	Theme Rose CONTACT Section
*/	
add_shortcode('rose-contact', 'rose_contact_section');
	function rose_contact_section($attr, $content=NULL){
		$contactAttr = extract(shortcode_atts(array(
			'title'			=>	'Contact Us',
			'subtitle'		=>	'Drop us some lines.',
			'social_title'	=>	'We are social'
		), $attr));
		ob_start(); ?>
			<section class="contact grey-bg section-area" id="contact">
			<div class="container">
				<div class="heading-wrap col-md-12">
					<h2 class="section-title"><?php echo $title; ?></h2>
					<span class="section-subtitle-m"><?php echo $subtitle; ?></span>
				</div>
				<div class="col-md-3 wow fadeInUp" data-wow-delay=".8s">
					<p>
						<?php echo do_shortcode($content); ?>
					</p>
					<div class="separator"></div>
					<h4 class="upper"><?php echo $social_title; ?></h4>
					<div class="m-social-list-icons">
						<span class="social-icon wow fadeInUp" data-wow-delay="1.2s"><a href="#"><i class="icon-facebook"></i></a></span>
						<span class="social-icon wow fadeInUp" data-wow-delay="1.4s"><a href="#"><i class="icon-twitter"></i></a></span>
						<span class="social-icon wow fadeInUp" data-wow-delay="1.6s"><a href="#"><i class="icon-googleplus"></i></a></span>
						<span class="social-icon wow fadeInUp" data-wow-delay="1.8s"><a href="#"><i class="icon-instagram"></i></a></span>
						<span class="social-icon wow fadeInUp" data-wow-delay="2s"><a href="#"><i class="icon-linkedin"></i></a></span>
					</div>
				</div>
				<!--  CONTACT FORM  -->
				<div class="col-md-4 col-md-offset-1 wow flipInY" data-wow-delay="1.2s">
					<div class="contact-form-wrap">
						<form role="form" id="contactForm" data-toggle="validator">
							<?php echo do_shortcode('[contact-form-7 id="176" title="Contact form 1"]'); ?>
						</form><!--  -->
					</div>
				</div>
				<!--  CONTACT INFO  -->
				<div class="col-md-3 col-md-offset-1 wow fadeInUp" data-wow-delay=".8s">
					<p>
						198  West 21th,<br>
						Street Suite 721 <br>
						New York, NY 10010
					</p>
					<div class="separator"></div>
					<p><i class="contact-icon icon-mail22"></i> <a class="__cf_email__" href="http://www.musamadesign.com/cdn-cgi/l/email-protection" data-cfemail="364f594344535b575f5a764f59434452595b575f581855595b">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></p>
					<p><i class="contact-icon icon-phone"></i> +88 (0) 101 0000 000</p>
				</div>
			</div>
		</section>
		<?php return ob_get_clean();
	}

