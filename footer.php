<section class="footer-container no-pd-b">
		<div class="footer-widgets container-fluid">
			<!-- ABOUT WIDGET -->
			
			
			<?php dynamic_sidebar('footer-widgets'); ?>
		</div>
		<footer class="footer col-md-12">
			<p><?php global $redux_rose; echo $redux_rose['footer-copyright']; ?></p>
		</footer>
	</section>
	<!-- *********************
		SEARCH
	********************** -->
	<div class="search-modal">
		<div class="container-fluid">
			<div class="search-modal-wrap">
				<input type="text" placeholder="SEARCH">
			</div>
			<div class="close-modal">
				<i class="icon-cross" id="searchClose"></i>
			</div>
		</div>
	</div>
	<!-- *********************
		CART
	********************** -->
	<div class="cart-modal">
		<div class="container-fluid">
			<div class="cart-modal-wrap col-md-4 col-md-offset-4">
				<div class="cart">
					<table class="table">
						<thead>
							<tr>
								<th></th>
								<th>Product</th>
								<th>Description</th>
								<th>Quantity</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><a href="#"><i class="cancel icon-cross"></i></a></td>
								<td><a href="#"><img src="images/shop/thumbs/1.jpg" alt=""></a></td>
								<td><span>Polo - black T-shirt</span></td>
								<td><span>1</span></td>
								<td><span>$24.99</span></td>
							</tr>
							<tr>
								<td><a href="#"><i class="cancel icon-cross"></i></a></td>
								<td><a href="#"><img src="images/shop/thumbs/2.jpg" alt=""></a></td>
								<td><span>Polo - black T-shirt</span></td>
								<td><span>1</span></td>
								<td><span>$24.99</span></td>
							</tr>
							<tr>
								<td><a href="#"><i class="cancel icon-cross"></i></a></td>
								<td><a href="#"><img src="images/shop/thumbs/3.jpg" alt=""></a></td>
								<td><span>Polo - black T-shirt</span></td>
								<td><span>1</span></td>
								<td><span>$24.99</span></td>
							</tr>
						</tbody>
					</table>
				</div><!-- cart -->
			</div>
			<div class="close-modal">
				<i class="icon-cross" id="cartClose"></i>
			</div>
		</div>
	</div>
	<div style="<?php global $redux_rose; echo "background: ". $redux_rose['backTotop']; ?>" class="back-to-top" id="backTop"><i class="icon-arrow-up4"></i></div>
	<?php wp_footer(); ?>
</body>



</html>