<?php get_header(); ?>
	<section class="heading-page-cover-section">
		<div class="container">
			<div class="col-md-12">
				<h1 class="heading-page-title"><?php global $redux_rose; echo $redux_rose['blog-title']; ?></h1>
				<span class="heading-page-subtitle"><?php global $redux_rose; echo $redux_rose['blog-subtitle']; ?></span>
			</div>
		</div>
	</section>
	<!-- *********************
		CONTENT
	********************** -->
	<section id="blog" class="content blog">
		<div class="container">
			<div class="blog-articles-container clearfix col-md-9">
				<!-- BLOG ARTICLE -->
				<?php if(have_posts()) : ?>
					<?php while(have_posts()): the_post(); ?>
						<?php get_template_part('formats/content', get_post_format()); ?>
					<?php endwhile; ?>

					<?php else:  ?>
						<h2>No Post Found</h2>
				<?php endif; ?>

				<!--  -->
				<!-- Pagination -->
				<?php 
					if(function_exists('the_posts_pagination')){
						the_posts_pagination(array(
							'type' 					=> 'list',
							'screen_reader_text'	=> ' ',
							'prev_text'				=> '<i class="icon-arrow-left5"></i>',
							'next_text'				=> '<i class="icon-arrow-right5"></i>'
						));
					}
				?>
				
			</div><!--  -->

			<!-- BLOG SIDEBAR -->
			
					
			<?php get_sidebar(); ?>
					
					
			
			
		</div>
	</section><!-- End Content -->

	<!-- *********************
		WIDGET CONTAINER
	********************** -->
<?php get_footer(); ?>