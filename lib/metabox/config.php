<?php 


add_action('cmb2_admin_init', 'rose_metabox');
	function rose_metabox(){
		
		$rose_box = new_cmb2_box(array(
			'title'			=> __('Format Additional Fields', 'rose'),
			'id'			=> 'rose-post-box',
			'object_types'	=> array('post'),
			
		));
		
		$rose_box->add_field(array(
			'id'	=> '_for-video',
			'type'	=> 'oembed',
			'name'	=> 'Video URL'
		));

		$rose_box->add_field(array(
			'id'	=> '_for-audio',
			'type'	=> 'oembed',
			'name'	=> 'Audio URL'
		));

		$rose_box->add_field(array(
			'id'	=> '_for-gallery',
			'type'	=> 'file_list',
			'name'	=> 'Gallery Images'
		));

		$sliders = new_cmb2_box(array(
			'title'			=> __("Additional Fields", 'rose'),
			'id'			=> 'additional-for-slider',
			'object_types'	=>	array('rose-slider'),
		));

		$sliders->add_field(array(
			'name'	=> 'Subtitle',
			'id'	=> '_slider-subtitle',
			'type'	=> 'text',
		));

		$sliders->add_field(array(
			'name'	=> 'Button Text',
			'id'	=> '_first-button-text',
			'type'	=> 'text',
		));
		$sliders->add_field(array(
			'name'	=> 'Button URL',
			'id'	=> '_first-button-url',
			'type'	=> 'text',
		));
		$sliders->add_field(array(
			'name'		=> 'Button Type',
			'id'		=> '_first-button-type',
			'type'		=> 'select',
			'options'	=> array(
				'sky'	=> 'Sky Button',
				'black'	=> 'Black Button'
			)
		));
		
	}
add_action('cmb2_admin_init', 'rose_brands_metabox');
	function rose_brands_metabox(){
		$rose_box = new_cmb2_box(array(
			'title'			=> __('Format Additional Fields', 'rose'),
			'id'			=> 'rose-brands-box',
			'object_types'	=> array('rose-brands'),
		));
		$rose_box->add_field(array(
			'id'	=> '_for-brands',
			'type'	=> 'file_list',
			'name'	=> 'Brand Images'
		));
	}
add_action('cmb2_admin_init', 'rose_client_metabox');
	function rose_client_metabox(){
		$rose_box = new_cmb2_box(array(
			'title'			=> __('Format Additional Fields', 'rose'),
			'id'			=> 'rose-client-box',
			'object_types'	=> array('rose-clients'),
		));
		$rose_box->add_field(array(
			'name'	=> 'Client Name',
			'id'	=> '_for-clients',
			'type'	=> 'text',
		));
	}
add_action('cmb2_admin_init', 'rose_team_metabox');
	function rose_team_metabox(){
		$rose_box = new_cmb2_box(array(
			'title'			=> __('Format Additional Fields', 'rose'),
			'id'			=> 'rose-team-box',
			'object_types'	=> array('rose-team'),
		));
		$rose_box->add_field(array(
			'name'	=> 'Team Member Name',
			'id'	=> '_for-teamMemberName',
			'type'	=> 'text',
		));
		$rose_box->add_field(array(
			'name'	=> 'Team Member Expert Skill',
			'id'	=> '_for-teamExpertSkill',
			'type'	=> 'text',
		));
	}
add_action('cmb2_admin_init', 'rose_team_social_link');
	function rose_team_social_link(){
		$rose_box = new_cmb2_box(array(
			'title'			=> __('Social Media Fields', 'rose'),
			'id'			=> 'rose-team-social',
			'object_types'	=> array('rose-team'),
		));
		$rose_box->add_field(array(
			'name'	=> 'Facebook icon',
			'id'	=> '_for-facebookicon',
			'type'	=> 'text'
		));
		$rose_box->add_field(array(
			'name'	=> 'Twitter icon',
			'id'	=> '_for-twittericon',
			'type'	=> 'text'
		));
		$rose_box->add_field(array(
			'name'	=> 'Skype icon',
			'id'	=> '_for-skypeicon',
			'type'	=> 'text'
		));
		$rose_box->add_field(array(
			'name'	=> 'flickr icon',
			'id'	=> '_for-flickricon',
			'type'	=> 'text'
		));
	}
add_action('cmb2_admin_init', 'rose_services_icon');
	function rose_services_icon(){
		$rose_box = new_cmb2_box(array(
			'title'			=> __('Service Icon Fields', 'rose'),
			'id'			=> 'rose-services-icon',
			'object_types'	=> array('rose-services'),
		));
		$rose_box->add_field(array(
			'name'	=> 'service Icon',
			'id'	=> '_for-serviceIcon',
			'type'	=> 'text',
		));
	}




















